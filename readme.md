### I'm fast <img src="https://img.shields.io/badge/pageSpeed-100/100-brightgreen.svg?style=flat"/>

- https://tools.pingdom.com/#!/cSNVym/http://ntobo.com/
- https://developers.google.com/speed/pagespeed/insights/?url=ntobo.com&tab=mobile


## Installation Instructions
1. Install node.js, NPM, and Gulp if not already installed.
2. run `npm install`.
4. Serve directory with your favorite dev server (http-server is great; port: 8081).
3. run `gulp` for production, `gulp dev` for development with BrowserSync (port: 3000).
